#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QColor>
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui
{
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget* parent = nullptr);
	~MainWindow();

private:
	void log(QColor color, QString text);

	inline void logI(QString text)
	{
		log(Qt::gray, text);
	}

	inline void logW(QString text)
	{
		log(Qt::yellow, text);
	}

	inline void logE(QString text)
	{
		log(Qt::red, text);
	}

private:
	void initilize();

private:
	Ui::MainWindow* ui;
};
#endif // MAINWINDOW_H
