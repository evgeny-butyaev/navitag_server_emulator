cmake_minimum_required(VERSION 3.5)

project(navitag_server_emulator LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(Protobuf 3.0 REQUIRED)
find_package(Qt5 5.10 COMPONENTS Widgets Network REQUIRED)

protobuf_generate_cpp(PROTO_SRCS PROTO_HDRS ${CMAKE_SOURCE_DIR}/tracker.proto)
set_property(SOURCE ${PROTO_SRCS} ${PROTO_HDRS} PROPERTY SKIP_AUTOGEN ON)

add_executable(${PROJECT_NAME} WIN32 MACOSX_BUNDLE
	main.cpp
	main_window.cpp
	main_window.h
	main_window.ui
	console_widget.h
	console_widget.cpp
	${PROTO_SRCS}
	${PROTO_HDRS}
	)

set_target_properties(${PROJECT_NAME} PROPERTIES
	AUTOUIC ON
	AUTOMOC ON
	AUTORCC ON
	CXX_STANDARD 17
	CXX_STANDARD_REQUIRED ON
	CXX_EXTENSIONS OFF
	MACOSX_BUNDLE_BUNDLE_NAME "Navitag Server Emulator"
	)

target_include_directories(${PROJECT_NAME} PRIVATE
	${CMAKE_CURRENT_BINARY_DIR}
	${Protobuf_INCLUDE_DIRS}
	)

target_link_libraries(${PROJECT_NAME} PRIVATE
	Qt5::Widgets
	Qt5::Network
	${Protobuf_LIBRARIES}
	)
