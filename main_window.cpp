#include "main_window.h"
#include "./ui_main_window.h"

#include <QFont>
#include <QMetaObject>
#include <QSettings>

#define MAINWINDOW_GROUP QStringLiteral("Main Window")
#define GEOMETRY_KEY QStringLiteral("Position")
#define STATE_KEY QStringLiteral("State")

MainWindow::MainWindow(QWidget* parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	QSettings settings;
	settings.beginGroup(MAINWINDOW_GROUP);
	restoreGeometry(settings.value(GEOMETRY_KEY).toByteArray());
	restoreState(settings.value(STATE_KEY).toByteArray());
	settings.endGroup();

	QMetaObject::invokeMethod(this, &MainWindow::initilize, Qt::QueuedConnection);
}

MainWindow::~MainWindow()
{
	QSettings settings;
	settings.beginGroup(MAINWINDOW_GROUP);
	settings.setValue(GEOMETRY_KEY, saveGeometry());
	settings.setValue(STATE_KEY, saveState());
	settings.endGroup();

	delete ui;
}

void MainWindow::log(QColor color, QString text)
{
	QMetaObject::invokeMethod(
		this, [c = ui->console, color, text]() {
			c->addLine(text, QFont::Bold, color);
		},
		Qt::QueuedConnection);
}

void MainWindow::initilize()
{
}
