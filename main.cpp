#include "main_window.h"

#include <QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	a.setApplicationName(QStringLiteral("Navitag Server Emulator"));
	a.setOrganizationName(QStringLiteral("IconSoft"));

	MainWindow w;
	w.show();
	return a.exec();
}
